## $ crontab -e
## @reboot python3 hydro_ml_firmware/Bootloader.py
## @reboot python3 hydro_ml_firmware/BootWatchdog.py

import subprocess
import os
import time
import uuid

try:
    os.system('pwd')
    os.chdir("/home/quanta/hydro_ml_firmware/")
    os.system('pwd')
except:
    pass
    
try:
    print('Load..')
    os.system('cat System_Env.py')
    import System_Env
    # print("\nUUID = {}\n".format(System_Env.UUID))
except:
    default_uuid = '4e4e27f1-9946-4a06-89eb-999999999999'
    os.system('cp System_Env.bac System_Env.py')
    print('!!! Create System_Env.py')
    import System_Env
    ############ Generate New UUID ########################
    try:
        os.system('cat System_Env.py')
        UUID = System_Env.UUID
        # print('\n\nUUID:{}'.format(UUID))
        # print(type(UUID))
        if UUID == default_uuid:
            print('Generate new uuid')
            New_uuid = str(uuid.uuid4())
            New_uuid = 'UUID = "'+ New_uuid + '"\n'
            a_file = open('System_Env.py', "r") #type string
            list_of_lines = a_file.readlines()
            print('Old :{}'.format(list_of_lines[0]))
            list_of_lines[0] = New_uuid
            print(list_of_lines[0])
            a_file = open('System_Env.py', "w")
            a_file.writelines(list_of_lines)
            a_file.close()
            print('Generate sucess:{}\nReboot system'.format(New_uuid))
            time.sleep(3)
            os.system('cat System_Env.py')
            os.system('sudo reboot')

    except Exception as e:
        print('Err:{}'.format(e))
        pass
        
import LogService as LogService
log_service = LogService.LogService()
import PIO_ctl
PIO_ctl.sys_blink(1)
import Constants
import timerService
import _thread
import MqttService as MqttService
# import SendData
import Sensor_service
import SendSensorData

def check_inet():
    #### Check Internet ####
    if check_connection.checkInternetHttplib():
        Constants.INET_CON = True
        return True
    else:
        Constants.INET_CON = False
        return False

time.sleep(0.5)
PIO_ctl.sys_blink(2)

try:
    print('Load..')
    os.system('cat System_Env.py')
    import System_Env
    # print("\nUUID = {}\n".format(System_Env.UUID))
except:
    default_uuid = '4e4e27f1-9946-4a06-89eb-999999999999'
    os.system('cp System_Env.bac System_Env.py')
    print('!!! Create System_Env.py')
    import System_Env
    ############ Generate New UUID ########################
    try:
        os.system('cat System_Env.py')
        UUID = System_Env.UUID
        # print('\n\nUUID:{}'.format(UUID))
        # print(type(UUID))
        if UUID == default_uuid:
            print('Generate new uuid')
            New_uuid = str(uuid.uuid4())
            New_uuid = 'UUID = "'+ New_uuid + '"\n'
            a_file = open('System_Env.py', "r") #type string
            list_of_lines = a_file.readlines()
            print('Old :{}'.format(list_of_lines[0]))
            list_of_lines[0] = New_uuid
            print(list_of_lines[0])
            a_file = open('System_Env.py', "w")
            a_file.writelines(list_of_lines)
            a_file.close()
            print('Generate sucess:{}\nReboot system'.format(New_uuid))
            time.sleep(3)
            os.system('cat System_Env.py')
            # os.system('sudo reboot')

    except Exception as e:
        print('Err:{}'.format(e))
        pass
    pass

time.sleep(0.5)
PIO_ctl.sys_blink(3)

try:
    import Feed_count_service
    # Feed_count_service.f_Service()
except Exception as e:
    print('Feed_count_service Err:{}'.format(e))
    pass

try:
    import Crop_service
except Exception as e:
    print('Crop_service Err:{}'.format(e))
    pass

try:
    import Constants
    import timerService
    import _thread
    import MqttService as MqttService
    # import SendData
    import Sensor_service
    import SendSensorData
    
    def check_inet():
        #### Check Internet ####
        if check_connection.checkInternetHttplib():
            Constants.INET_CON = True
            return True
        else:
            Constants.INET_CON = False
            return False
    
    import check_connection
    
    ####################### Get CPU Info
    try:
        print('get cpuinfo')
        ls_cpuinfo = subprocess.run(['tail','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
        Constants.CPUINFO = ls_cpuinfo.stdout
        file = open('cpuinfo.txt', "w+b")
        # os.system('ls -la')
        file.write(ls_cpuinfo.stdout)
        file.close()
        log_service.info('-Create file cpuinfo')
        print('*** Create file cpuinfo')
        ################ write cpu model to Constansts
        ls_cpuinfo = subprocess.run(['tail','-1','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
        Constants.MODEL = ls_cpuinfo.stdout
    except:
        pass
    
    name = os.uname()
    MachineNodename = name.nodename
    print(MachineNodename)
except Exception as e:
    str_ = '\nErr2:{}'.format(e)
    print(str_)
    print(type(str_))
    b = bytes(str_, 'utf-8')
    file = open('err_2.txt', "w+b")
    file.write(b)
    file.close()
    pass

time.sleep(1)
PIO_ctl.sys_blink(4)

class main:
    def __init__(self):
        print(Constants.SOFTWARE_TITLE)
        time.sleep(0.5)
        try:
            # if System_Env.TIMER_SERVICE == True:
            #     _thread.start_new_thread(timerService.timerService, ())
            #     log_service.info('-Run timerService')
            #     print('-Run timerService')
            while True:
                time.sleep(0.5)
                if check_inet():
                    _thread.start_new_thread(MqttService.MqttService, ())
                    log_service.info('-### Run MqttService')
                    print('\n-### Run MqttService')
                        
                    _thread.start_new_thread(Sensor_service.SensorService , ())
                    log_service.info('-### Run Sensor_service')
                    print('\n-### Run Sensor_service')

                    _thread.start_new_thread(Feed_count_service.f_Service , ())
                    log_service.info('-### Run Feed_count_service')
                    print('\n-### Run Feed_count_service')

                    _thread.start_new_thread(Crop_service.C_Service , ())
                    log_service.info('-### Run Crop_service')
                    print('\n-### Crop_service')
                    
                    _thread.start_new_thread(timerService.timerService, ())
                    log_service.info('-Run timerService')
                    print('-Run timerService')

                    _thread.start_new_thread(SendSensorData.SendSensorData(), ())
                    log_service.info('-### Run Send Sensor Data Service')
                    print('\n-### Run SendDataService')
                    break
                else:
                    print('!!Internet Not Connect')
                    log_service.info('!!Internet Not Connect')
                    time.sleep(3)
        except Exception as e:
            print('!!!! Err:{}'.format(e))
            log_service.info('!!!! Err:{}'.format(e))
            pass

        while True:
            time.sleep(15)
            print('Run on System Main loop')
            PIO_ctl.sys_blink(2)
    
        print('Exit Bootloader main')

if __name__ == '__main__':
    main()
